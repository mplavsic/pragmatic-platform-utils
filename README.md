A library package that simplifies the cross-platform development process by providing the following declarative and pragmatic instruments:

* [`platformFunction`](#platformfunction) 
* [`PlatformBuilder`](#platformbuilder) 
* [`AdaptivePlatformBuilder`](#adaptiveplatformbuilder) 

Additionally, it also provides [an experimental and declarative `Platform`](#platform).

# Getting started

You need to add `pragmatic_platform_utils` to your dependencies.

```yaml
dependencies:
  pragmatic_platform_utils: ^latest # replace latest with version number
```

To use it, import `package:pragmatic_platform_utils/pragmatic_platform_utils.dart`.

# Notion prerequisite

## Platforms and platform groups
_____________________________________

| *Platform* | apple | mobile | desktop |
| ---------- |:-----:|:------:|:-------:|
| android    |       | ✓      |         |
| ios        | ✓     | ✓      |         |
| linux      |       |        | ✓       |
| macos      | ✓     |        | ✓       |
| windows    |       |        | ✓       |
| web        |       |        |         |
| fuchsia    |       |        |         |

Priority order: from left to right.

## Subplatforms, their platforms and platform groups

Subplatforms are screen-size inferrable platforms, such as `ipadSmall`. They are only relevant for [`AdaptivePlatformBuilder`](#adaptiveplatformbuilder).

`androidTablet`, `ipad` and `webTablet` are subgroups having precedence over the platforms.

`apple`, `tabletSmall`, ... are subgroups that will be checked before resorting to the `other` function/builder.

| *Subplatform*      | <div style="color:green;">androidTablet</div><div style="color:fuchsia;">ipad</div><div style="color:orange;">webTablet</div> | *Platform* | apple | <div style="color:red;">tabletSmall</div><div style="color:gold;">tabletLarge</div> | tablet | mobile | desktop |
| ------------------ |:-----------------------------------------------------------------------------------------------------------------------------:| ---------- |:-----:|:-----------------------------------------------------------------------------------:|:------:|:------:|:-------:|
| androidSmartphone  |                                                                                                                               | android    |       |                                                                                     |        | ✓      |         |
| androidTabletSmall | <div style="color:green;">✓</div>                                                                                             | android    |       | <div style="color:red;">✓</div>                                                     | ✓      | ✓      |         |
| androidTabletLarge | <div style="color:green;">✓</div>                                                                                             | android    |       | <div style="color:gold;">✓</div>                                                    | ✓      | ✓      |         |
| iphone             |                                                                                                                               | ios        | ✓     |                                                                                     |        | ✓      |         |
| ipadSmall          | <div style="color:fuchsia;">✓</div>                                                                                           | ios        | ✓     | <div style="color:red;">✓</div>                                                     | ✓      | ✓      |         |
| ipadLarge          | <div style="color:fuchsia;">✓</div>                                                                                           | ios        | ✓     | <div style="color:gold;">✓</div>                                                    | ✓      | ✓      |         |
| webSmartphone      |                                                                                                                               | web        |       |                                                                                     |        | ✓      |         |
| webTabletSmall     | <div style="color:orange;">✓</div>                                                                                            | web        |       | <div style="color:red;">✓</div>                                                     | ✓      | ✓      |         |
| webTabletLarge     | <div style="color:orange;">✓</div>                                                                                            | web        |       | <div style="color:gold;">✓</div>                                                    | ✓      | ✓      |         |
| webDesktop         |                                                                                                                               | web        |       |                                                                                     |        |        | ✓       |
|                    |                                                                                                                               | *linux*    |       |                                                                                     |        |        | *✓*     |
|                    |                                                                                                                               | *macos*    | *✓*   |                                                                                     |        |        | *✓*     |
|                    |                                                                                                                               | *windows*  |       |                                                                                     |        |        | *✓*     |
|                    |                                                                                                                               | *fuchsia*  |       |                                                                                     |        |        |         |

Priority order: from left to right.

NB: The developers don't have to specify any breakpoints, since they are already taken care of by this library. Some examples: 
- `ipadSmall` targets all different generations of iPad mini.
- `ipadLarge` targets all other iPads (e.g. Air, Pro, ...).
- `ipad` targets all iPad models.

Breakpoints might also not be the same across platforms, e.g. iOS might require a screen width (in pixels) to correctly switch from iPhone to iPad that is different than Android's required screen width (from smartphone to tablet). Again, all of this is already taken care of by this library.

It might be possible that those breakpoints are incorrect on some devices; in those cases please open an issue ;)

# Usage

## platformFunction

`platformFunction` is a declarative pattern to invoke the right function for 
the right platform, at runtime. If the goal is only to select a value, a function
expression is what needs to be passed, e.g., `() => 6`.

When constructing widgets, [`PlatformBuilder`](#platformbuilder) should be preferred.

Usage example:

```dart
final res = platformFunction(
  other: () => 6,
  android: () => 10,
  macos: () => 11,
);
```

`res` will have value 6 on all platforms (including web), except for Android and macOS.

### Usage with platform groups

In the above example, both `android` and `macos` are exact platforms. However, often all is needed is a desktop/mobile/(web?) distinction.

Let's see an example:

```dart
final res = platformFunction(
  mobile: () => 6,
  other: () => 10, // all unspecified platforms
);
```

If you want to be more clear, you can also pass the argument `desktop`.

```dart
final res = platformFunction(
  mobile: () => 6,
  desktop: () => 10,
  other: () => 10, // it will be invoked only on web and fuchsia
);
```

Let's assume the app you are building only targets `desktop` and `mobile` (therefore no `web` and `fuchsia`). In this case, the `other` argument is a bit superfluous, since all cases are already covered by `mobile` and `desktop`. Even though not recommended, it is possible to use the unsafe variant in cases like this one. More concretely:

```dart
final res = unsafePlatformFunction(
  mobile: () => 6,
  desktop: () => 10,
  // you can omit `other` if we really know what you are doing
);
```


## PlatformBuilder

`PlatformBuilder` is one solution this library offers for building widgets. It uses the unsafe variant of `platformFunction` internally. This builder should be used when the widget has a dependency only on the platform (e.g. how a "share photo" button should look like). However, in some cases the widget might have a dependency on the screen dimension of the device, i.e. window resizing or split view should change the layout (e.g. the page scaffold displays a `BottomNavigationBar` on smartphones, however it needs to switch to a `NavigationRail` on tablets). In those cases, [`AdaptivePlatformBuilder`](#adaptiveplatformbuilder) should be used.

### PlatformBuilder in action

The following snippets should be self-explanatory.

```dart
PlatformBuilder(
  other: () => Text('other'),
  mobile: () => Text('mobile'),
);
```

```dart
PlatformBuilder(
  other: () => Text('other'),
  desktop: () => Text('desktop'),
  linux: () => Text('linux'),
);
// 'desktop' will be displayed only on macOS and Windows
```

```dart
PlatformBuilder(
  other: () => Text('other'),
  apple: () => Text('apple'),
  ios: () => Text('ios'),
);
// 'apple' will be displayed only on macOS
```

```dart
PlatformBuilder(
  other: () => Text('other'),
  mobile: () => Text('mobile'),
  apple: () => Text('apple'),
  ios: () => Text('ios'),
  desktop: () => Text('desktop'),
  linux: () => Text('linux'),
);
```

The default constructor in the following example requires passing the argument `other`, even though its expression will never be invoked.

```dart
PlatformBuilder(
  mobile: () => Text('mobile'),
  desktop: () => Text('desktop'),
  web: () => Text('web'),
  fuchsia: () => Text('fuchsia'),
  other: () => Text("other"),
);
```

To some, this might look a bit ugly. It is possible to fix this with the `unsafe` constructor, which assumes that the developer knows what they are doing.

```dart
PlatformBuilder.unsafe(
  mobile: () => Text('mobile'),
  desktop: () => Text('desktop'),
  web: () => Text('web'),
  fuchsia: () => Text('fuchsia'),
);
```

## AdaptivePlatformBuilder

The widget can be used anywhere in the code, i.e., not necessarily only at the scaffold level. Internally, it includes a `LayoutBuilder`, implying a different widget will be rebuilt once a breakpoint is reached, e.g., `webSmartphone` to `webTablet` when the window is resized.

Usage examples:

```dart
AdaptivePlatformBuilder(
  other: () => Text('other'),
  ipad: () => Text('ipad'),
  androidSmartphone: () => Text('androidSmartphone'),
  android: () => Text('android'),
);
// iPhones will display 'other', while iPads 'ipad' (unless the app is resized to look like an iPhone app, in that case 'other' will be displayed).
// Similarly, Android smartphones will display 'androidSmartphone', while Android tablets will display 'android'.
```

You can use the `unsafe` constructor if you know what you are doing.

```dart
AdaptivePlatformBuilder.unsafe(
  web: () => Text('web'),
  fuchsia: () => Text('fuchsia'),
  ipad: () => Text('ipad'),
  androidSmartphone: () => Text('androidSmartphone'),
  android: () => Text('android'),
);
```

# Experimental functionality

This package also contains an experimental `Platform`. However, I think that [`platformFunction`](#platformbuilder) and [`PlatformBuilder`](#platformbuilder) offer a much neater solution.

## Platform

If using [`platformFunction`](#platformbuilder) and [`PlatformBuilder`](#platformbuilder) simply doesn't feel natural in some parts of your code, you can use this library's `Platform` class, which allows to write more declarative cross-platform layouts' code than what the standard `Platform` already offers. It is however less declarative than [`platformFunction`](#platformbuilder) and [`PlatformBuilder`](#platformbuilder).

You can distinguish between platforms more easily and naturally when writing layouts. No more checking for `kIsWeb` first, like in:

```dart
// typical Dart
if (kIsWeb) { // web always gets checked first
  return webValue;
} else {
  if (Platform.isAndroid) {
    return androidValue;
  } ...
}
```

Instead, once you import `package:pragmatic_platform_utils/platform.dart` you can write:

```dart
// this library
build(BuildContext context) {
  if (Platform.isAndroid) {
    return WidgetForAndroid(context);
  } else if (Platform.isWeb) { // web doesn't have to be checked first
    return WidgetForWeb(context);
  } ...
}
```

`Platform` can obviously be used also outside a widget:

```dart
getLink() {
  if (Platform.isAndroid) {
    return 'www.android.com';
  } else if (Platform.isWindows) {
    return 'www.windows.com';
  } ...
}
```

### Differentiate between this `Platform` class and the official one

Use the `as` keyword to differentiate between this `Platform` class and the official one.

```dart
import 'package:pragmatic_platform_utils/platform.dart';
import 'dart:io.dart' as typical;
```

Then you can write:

```dart
typical.Platform.isLinux // dart.io
Platform.isLinux // this library
```

### Platform constants and `isOneOf`

There are 7 constant instances of `Platform` that help ensure type safety. They are defined top level as:

* `android`
* `fuchsia`
* `ios`
* `linux`
* `macos`
* `windows`
* `web`

The constructor is private and more instances cannot be created.

Thanks to these constants, we can improve the following standard Dart code:

```dart
if (kIsWeb || Platform.isLinux || Platform.isWindows) {...}
```

to:

```dart
if (Platform.isOneOf([linux, windows, web])) {...}
// again, web is not necessarily first

// notice how this would not be type safe, if the method accepted a list of strings:
if (Platform.isOneOf(['linux', 'window', 'web'])) {...}
// (did you see the typo?)
```

### getRunningPlatform and type safety

Usually, you should select the right value (or function to invoke) this way:

```dart
if (Platform.isAndroid) {
  return androidValue;
} else if (Platform.isWeb) {
  return webValue;
}
return defaultValue;
```

At the moment, the switch-case statement cannot be used with `Platform`, therefore only `if`/`else if`/`else` statements are valid.

### No platform groups support

Support for platform groups is not planned.

# Internal functionality not directly accessible
## adaptivePlatformFunction

`adaptivePlatformFunction` and `unsafeAdaptivePlatformFunction` work analogously to [`platformFunction`](#platformfunction) and `unsafePlatformFunction`. What differs is the introduction of subplatforms (i.e., screen-size inferrable platforms) and more platform/subplatform groups. Also, the function take the context as the parameter because they internally use MediaQuery to get the device height and width, which is needed for choosing the right subplatform.

Practically, one should not ever consider using this function, since [`AdaptivePlatformBuilder`](#adaptiveplatformbuilder) is the way to go for constructing widgets, and it is pretty unlikely one would need a function like this otherwise. [`AdaptivePlatformBuilder`](#adaptiveplatformbuilder) uses the unsafe variant of `adaptivePlatformFunction` internally.

NB: Unlike [`AdaptivePlatformBuilder`](#adaptiveplatformbuilder) (which thanks to `LayoutBuilder` rebuilds), this function won't be invoked again automatically in case the layout changes.

# Possible improvements

- Test and update/correct breakpoint values, per platform.
- Add `desktop` breakpoints too.