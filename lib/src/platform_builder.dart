import 'package:flutter/widgets.dart';

import 'platform_function.dart';

/// Builds a [Widget] based on the platform the application is
/// currently running on.
class PlatformBuilder extends StatelessWidget {
  // Normal platforms
  final Widget Function()? other;
  final Widget Function()? android;
  final Widget Function()? fuchsia;
  final Widget Function()? ios;
  final Widget Function()? linux;
  final Widget Function()? macos;
  final Widget Function()? windows;
  final Widget Function()? web;

  // Platform groups
  final Widget Function()? apple;
  final Widget Function()? desktop;
  final Widget Function()? mobile;

  /// Selects the widget builder to invoke based on the platform the
  /// application is currently running on.
  ///
  /// If provided, [other] will also be selected as default (fallback) at
  /// runtime in case the running platform (or a platform group containing it)
  /// is not specified. [other] will also be used if the platform is not yet
  /// supported by Flutter or this plugin. If [other] is not assigned, the
  /// function might be unsafe. See the last docstring paragraph.
  ///
  /// **Platform groups**
  ///
  /// [apple], [desktop] and [mobile] are platform groups.
  ///
  /// - [mobile] will be selected if, at runtime, the platform is one of:
  ///   - [android]
  ///   - [ios]
  /// - [desktop] will be selected if, at runtime, the platform is one of:
  ///   - [linux]
  ///   - [macos]
  ///   - [windows]
  /// - [apple] will be selected if, at runtime, the platform is one of:
  ///   - [ios]
  ///   - [macos]
  ///
  /// Platforms have precedence over platform groups. [apple] has precedence
  /// over both [desktop] and [mobile].
  ///
  /// **Scope**
  ///
  /// Use this builder for both:
  ///
  /// * function (business logic)
  /// * appearance (view)
  ///
  /// **Remarks**
  ///
  /// This is an unsafe builder and should not be used unless all targeted
  /// platforms are given a builder. The safe constructor variant
  /// [PlatformBuilder] enforces the assignment of [other]. If using the unsafe
  /// variant, i.e. this constructor, ensure all possible cases are already
  /// taken into account, i.e. there is no need to specify [other].
  const PlatformBuilder.unsafe({
    Key? key,
    this.other,
    this.android,
    this.fuchsia,
    this.ios,
    this.linux,
    this.macos,
    this.windows,
    this.web,
    this.apple,
    this.desktop,
    this.mobile,
  }) : super(key: key);

  /// Selects the widget builder to invoke based on the platform the
  /// application is currently running on.
  ///
  /// [other] will also be selected as default (fallback) at runtime in case the
  /// running platform (or a platform group containing it) is not specified.
  /// [other] will also be used if the platform is not yet supported by Flutter
  /// or this plugin.
  ///
  /// **Platform groups**
  ///
  /// [apple], [desktop] and [mobile] are platform groups.
  ///
  /// - [mobile] will be selected if, at runtime, the platform is one of:
  ///   - [android]
  ///   - [ios]
  /// - [desktop] will be selected if, at runtime, the platform is one of:
  ///   - [linux]
  ///   - [macos]
  ///   - [windows]
  /// - [apple] will be selected if, at runtime, the platform is one of:
  ///   - [ios]
  ///   - [macos]
  ///
  /// Platforms have precedence over platform groups. [apple] has precedence
  /// over both [desktop] and [mobile].
  ///
  /// **Scope**
  ///
  /// Use this builder for both:
  ///
  /// * function (business logic)
  /// * appearance (view)
  const PlatformBuilder({
    Key? key,
    required Widget Function() other,
    Widget Function()? android,
    Widget Function()? fuchsia,
    Widget Function()? ios,
    Widget Function()? linux,
    Widget Function()? macos,
    Widget Function()? windows,
    Widget Function()? web,
    Widget Function()? apple,
    Widget Function()? desktop,
    Widget Function()? mobile,
  }) : this.unsafe(
          other: other,
          android: android,
          apple: apple,
          desktop: desktop,
          fuchsia: fuchsia,
          ios: ios,
          linux: linux,
          macos: macos,
          mobile: mobile,
          web: web,
          windows: windows,
        );

  @override
  Widget build(BuildContext context) {
    return unsafePlatformFunction(
      other: other,
      android: android,
      apple: apple,
      desktop: desktop,
      fuchsia: fuchsia,
      ios: ios,
      linux: linux,
      macos: macos,
      mobile: mobile,
      web: web,
      windows: windows,
    );
  }
}
