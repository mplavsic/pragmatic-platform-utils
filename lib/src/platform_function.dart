import '../platform.dart';

import 'platform_builder.dart';

/// Immediately invokes a function based on the platform the application is
/// currently running on.
///
/// If provided, [other] will also be selected as default (fallback) at
/// runtime in case the running platform (or a platform group containing it)
/// is not specified. [other] will also be used if the platform is not yet
/// supported by Flutter or this plugin. If [other] is not assigned, the
/// function might be unsafe. See the last docstring paragraph.
///
/// **Platform groups**
///
/// [apple], [desktop] and [mobile] are platform groups.
///
/// - [mobile] will be selected if, at runtime, the platform is one of:
///   - [android]
///   - [ios]
/// - [desktop] will be selected if, at runtime, the platform is one of:
///   - [linux]
///   - [macos]
///   - [windows]
/// - [apple] will be selected if, at runtime, the platform is one of:
///   - [ios]
///   - [macos]
///
/// Platforms have precedence over platform groups. [apple] has precedence
/// over both [desktop] and [mobile].
///
/// **Scope**
///
/// Use this function for both:
///
/// * function (business logic)
/// * appearance (view)
///
/// **Remarks**
///
/// NB: prefer [PlatformBuilder.unsafe] when constructing a widget.
///
/// This is an unsafe function and should not be used unless all targeted
/// platforms can be reached. The safe variant [platformFunction] enforces
/// the assignment of [other]. If using the unsafe variant, i.e. this
/// function, ensure all possible cases are already taken into
/// account, i.e. there is no need to specify [other].
T unsafePlatformFunction<T>({
  T Function()? other,
  T Function()? android,
  T Function()? fuchsia,
  T Function()? ios,
  T Function()? linux,
  T Function()? macos,
  T Function()? windows,
  T Function()? web,
  T Function()? apple,
  T Function()? desktop,
  T Function()? mobile,
}) {
  if (Platform.isAndroid) {
    return android?.call() ?? mobile?.call() ?? other!.call();
  } else if (Platform.isIOS) {
    return ios?.call() ?? apple?.call() ?? mobile?.call() ?? other!.call();
  } else if (Platform.isLinux) {
    return linux?.call() ?? desktop?.call() ?? other!.call();
  } else if (Platform.isMacOS) {
    return macos?.call() ?? apple?.call() ?? desktop?.call() ?? other!.call();
  } else if (Platform.isWindows) {
    return windows?.call() ?? desktop?.call() ?? other!.call();
  } else if (Platform.isWeb) {
    return web?.call() ?? other!.call();
  } else if (Platform.isFuchsia) {
    return fuchsia?.call() ?? other!.call();
  }
  return other!.call();
}

/// Immediately invokes a function based on the platform the application is
/// currently running on.
///
/// [other] will also be selected as default (fallback) at runtime in case the
/// running platform (or a platform group containing it) is not specified.
/// [other] will also be used if the platform is not yet supported by Flutter or
/// this plugin.
///
/// **Platform groups**
///
/// [apple], [desktop] and [mobile] are platform groups.
///
/// - [mobile] will be selected if, at runtime, the platform is one of:
///   - [android]
///   - [ios]
/// - [desktop] will be selected if, at runtime, the platform is one of:
///   - [linux]
///   - [macos]
///   - [windows]
/// - [apple] will be selected if, at runtime, the platform is one of:
///   - [ios]
///   - [macos]
///
/// Platforms have precedence over platform groups. [apple] has precedence
/// over both [desktop] and [mobile].
///
/// **Scope**
///
/// Use this function for both:
///
/// * function (business logic)
/// * appearance (view)
///
/// **Remarks**
///
/// NB: prefer [PlatformBuilder] when constructing a widget.
T platformFunction<T>({
  required final T Function() other,
  final T Function()? android,
  final T Function()? fuchsia,
  final T Function()? ios,
  final T Function()? linux,
  final T Function()? macos,
  final T Function()? windows,
  final T Function()? web,
  final T Function()? apple,
  final T Function()? desktop,
  final T Function()? mobile,
}) =>
    unsafePlatformFunction(
      other: other,
      android: android,
      fuchsia: fuchsia,
      ios: ios,
      linux: linux,
      macos: macos,
      windows: windows,
      web: web,
      apple: apple,
      desktop: desktop,
      mobile: mobile,
    );
