import 'package:flutter/cupertino.dart';

import '../platform.dart';

import 'adaptive_platform_builder.dart';

/// Immediately invokes a function based on the platform the application is
/// currently running on.
///
/// If provided, [other] will also be selected as default (fallback) at runtime
/// in case the running platform (or a platform group containing it) is not
/// specified. [other] will also be used if the platform is not yet supported by
/// Flutter or this plugin. If [other] is not assigned, the function might be
/// unsafe. See the last docstring paragraph.
///
/// **Platform groups and subplatforms**
///
/// To find out all the possible combinations, the
/// [documentation](https://gitlab.com/mplavsic/pragmatic-platform-utils)
/// should be consulted.
///
/// **Scope**
///
/// Use this function only for:
///
/// * appearance (view)
///
/// Since there is no other method to infer one of the above subplatforms
/// other than screen size dimension, you should **NEVER** rely on this function
/// for business logic differences (such as the path to the database) between
/// siblings platforms, e.g., [androidSmartphone] and [androidTablet]. The
/// app dimension could change at runtime (app resizing), resulting in a
/// platform switch, e.g., [androidSmartphone] to [androidTablet].
///
/// **Remarks**
///
/// NB: When constructing a widget, prefer [AdaptivePlatformBuilder.unsafe] over
/// invoking this method in the build.
///
/// This is an unsafe function and should not be used unless all targeted
/// platforms can be reached. The safe variant [adaptivePlatformFunction]
/// enforces the assignment of [other]. If using the unsafe variant, i.e. this
/// function, ensure all possible cases are already taken into
/// account, and the [other] parameter would be redundant.
T unsafeAdaptivePlatformFunction<T>({
  required final BuildContext context,
  final T Function()? other,
  final T Function()? mobile,
  final T Function()? tablet,
  final T Function()? tabletSmall,
  final T Function()? tabletLarge,
  final T Function()? android,
  final T Function()? androidSmartphone,
  final T Function()? androidTablet,
  final T Function()? androidTabletSmall,
  final T Function()? androidTabletLarge,
  final T Function()? fuchsia,
  final T Function()? apple,
  final T Function()? ios,
  final T Function()? iphone,
  final T Function()? ipad,
  final T Function()? ipadSmall,
  final T Function()? ipadLarge,
  final T Function()? macos,
  final T Function()? linux,
  final T Function()? windows,
  final T Function()? web,
  final T Function()? webSmartphone,
  final T Function()? webTablet,
  final T Function()? webTabletSmall,
  final T Function()? webTabletLarge,
  final T Function()? webDesktop,
  final T Function()? desktop,
}) {
  final shortestSide = MediaQuery.of(context).size.shortestSide;
  final orientation = MediaQuery.of(context).orientation;
  if (Platform.isAndroid) {
    if ((shortestSide <= 600 && orientation == Orientation.portrait) ||
        (shortestSide <= 570 && orientation == Orientation.landscape)) {
      if (androidSmartphone != null) return androidSmartphone.call();
      if (android != null) return android.call();
    } else if ((shortestSide <= 900 && orientation == Orientation.portrait) ||
        (shortestSide <= 880 && orientation == Orientation.landscape)) {
      if (androidTabletSmall != null) return androidTabletSmall.call();
      if (androidTablet != null) return androidTablet.call();
      if (android != null) return android.call();
      if (tabletSmall != null) return tabletSmall.call();
      if (tablet != null) return tablet.call();
    } else {
      if (androidTabletLarge != null) return androidTabletLarge.call();
      if (androidTablet != null) return androidTablet.call();
      if (android != null) return android.call();
      if (tabletLarge != null) return tabletLarge.call();
      if (tablet != null) return tablet.call();
    }
    return mobile?.call() ?? other!.call();
  } else if (Platform.isIOS) {
    if ((shortestSide <= 600 && orientation == Orientation.portrait) ||
        (shortestSide <= 570 && orientation == Orientation.landscape)) {
      if (iphone != null) return iphone.call();
      if (ios != null) return ios.call();
      if (apple != null) return apple.call();
    } else if ((shortestSide <= 900 && orientation == Orientation.portrait) ||
        (shortestSide <= 880 && orientation == Orientation.landscape)) {
      if (ipadSmall != null) return ipadSmall.call();
      if (ipad != null) return ipad.call();
      if (ios != null) return ios.call();
      if (apple != null) return apple.call();
      if (tabletSmall != null) return tabletSmall.call();
      if (tablet != null) return tablet.call();
    } else {
      if (ipadLarge != null) return ipadLarge.call();
      if (ipad != null) return ipad.call();
      if (ios != null) return ios.call();
      if (apple != null) return apple.call();
      if (tabletLarge != null) return tabletLarge.call();
      if (tablet != null) return tablet.call();
    }
    return mobile?.call() ?? other!.call();
  } else if (Platform.isLinux) {
    return linux?.call() ?? desktop?.call() ?? other!.call();
  } else if (Platform.isMacOS) {
    return macos?.call() ?? apple?.call() ?? desktop?.call() ?? other!.call();
  } else if (Platform.isWindows) {
    return windows?.call() ?? desktop?.call() ?? other!.call();
  } else if (Platform.isWeb) {
    if ((shortestSide <= 640 && orientation == Orientation.portrait) ||
        (shortestSide <= 580 && orientation == Orientation.landscape)) {
      if (webSmartphone != null) return webSmartphone.call();
      if (web != null) return web.call();
      if (mobile != null) return mobile.call();
    } else if ((shortestSide <= 900 && orientation == Orientation.portrait) ||
        (shortestSide <= 880 && orientation == Orientation.landscape)) {
      if (webTabletSmall != null) return webTabletSmall.call();
      if (web != null) return web.call();
      if (tabletSmall != null) return tabletSmall.call();
      if (tablet != null) return tablet.call();
      if (mobile != null) return mobile.call();
    } else if ((shortestSide <= 1280 && orientation == Orientation.portrait) ||
        (shortestSide <= 1280 && orientation == Orientation.landscape)) {
      if (webTabletLarge != null) return webTabletLarge.call();
      if (web != null) return web.call();
      if (tabletLarge != null) return tabletLarge.call();
      if (tablet != null) return tablet.call();
      if (mobile != null) return mobile.call();
    } else {
      if (webDesktop != null) return webDesktop.call();
      if (web != null) return web.call();
      if (desktop != null) return desktop.call();
    }
    return other!.call();
  } else if (Platform.isFuchsia) {
    if (fuchsia != null) fuchsia.call();
    // Add breakpoints here in the future
    return other!.call();
  }
  return other!.call();
}

/// Immediately invokes a function based on the platform the application is
/// currently running on.
///
/// [other] will also be selected as default (fallback) at runtime in case the
/// running platform (or a platform group containing it) is not specified.
/// [other] will also be used if the platform is not yet supported by Flutter
/// or this plugin.
///
/// **Platform groups and subplatforms**
///
/// To find out all the possible combinations, the
/// [documentation](https://gitlab.com/mplavsic/pragmatic-platform-utils)
/// should be consulted.
///
/// **Scope**
///
/// Use this function only for:
///
/// * appearance (view)
///
/// Since there is no other method to infer one of the above subplatforms
/// other than screen size dimension, you should **NEVER** rely on this function
/// for business logic differences (such as the path to the database) between
/// siblings platforms, e.g., [androidSmartphone] and [androidTablet]. The
/// app dimension could change at runtime (app resizing), resulting in a
/// platform switch, e.g., [androidSmartphone] to [androidTablet].
///
/// **Remarks**
///
/// NB: When constructing a widget, prefer [AdaptivePlatformBuilder] over
/// invoking this function in the build.
T adaptivePlatformFunction<T>({
  required BuildContext context,
  required T Function() other,
  final T Function()? android,
  final T Function()? androidSmartphone,
  final T Function()? androidTablet,
  final T Function()? androidTabletSmall,
  final T Function()? androidTabletLarge,
  final T Function()? fuchsia,
  final T Function()? ios,
  final T Function()? linux,
  final T Function()? macos,
  final T Function()? windows,
  final T Function()? web,
  final T Function()? iphone,
  final T Function()? ipad,
  final T Function()? ipadSmall,
  final T Function()? ipadLarge,
  final T Function()? webSmartphone,
  final T Function()? webTablet,
  final T Function()? webTabletSmall,
  final T Function()? webTabletLarge,
  final T Function()? webLarge,
  final T Function()? apple,
  final T Function()? desktop,
  final T Function()? mobile,
  final T Function()? tablet,
  final T Function()? tabletSmall,
  final T Function()? tabletLarge,
}) =>
    unsafeAdaptivePlatformFunction(
      context: context,
      other: other,
      android: android,
      androidSmartphone: androidSmartphone,
      androidTablet: androidTablet,
      androidTabletSmall: androidTabletSmall,
      androidTabletLarge: androidTabletLarge,
      fuchsia: fuchsia,
      ios: ios,
      iphone: iphone,
      ipad: ipad,
      ipadSmall: ipadSmall,
      ipadLarge: ipadLarge,
      linux: linux,
      macos: macos,
      windows: windows,
      web: web,
      webSmartphone: webSmartphone,
      webTablet: webTablet,
      webTabletSmall: webTabletSmall,
      webTabletLarge: webTabletLarge,
      webDesktop: webLarge,
      apple: apple,
      desktop: desktop,
      mobile: mobile,
      tablet: tablet,
      tabletSmall: tabletSmall,
      tabletLarge: tabletSmall,
    );
