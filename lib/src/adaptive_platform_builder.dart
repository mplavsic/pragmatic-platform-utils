import 'package:flutter/material.dart';
import 'adaptive_platform_function.dart';

/// Builds a [Widget] based on the exact platform the application is
/// currently running on.
class AdaptivePlatformBuilder extends StatelessWidget {
  final Widget Function()? other;
  final Widget Function()? android;
  final Widget Function()? fuchsia;
  final Widget Function()? ios;
  final Widget Function()? linux;
  final Widget Function()? macos;
  final Widget Function()? windows;
  final Widget Function()? web;
  final Widget Function()? iphone;
  final Widget Function()? ipad;
  final Widget Function()? ipadSmall;
  final Widget Function()? ipadLarge;
  final Widget Function()? androidSmartphone;
  final Widget Function()? androidTablet;
  final Widget Function()? androidTabletSmall;
  final Widget Function()? androidTabletLarge;
  final Widget Function()? webSmartphone;
  final Widget Function()? webTablet;
  final Widget Function()? webTabletSmall;
  final Widget Function()? webTabletLarge;
  final Widget Function()? webDesktop;
  final Widget Function()? apple;
  final Widget Function()? desktop;
  final Widget Function()? mobile;
  final Widget Function()? tablet;
  final Widget Function()? tabletSmall;
  final Widget Function()? tabletLarge;

  /// Selects the widget builder to invoke based on the platform the
  /// application is currently running on.
  ///
  /// If provided, [other] will also be selected as default (fallback) at
  /// runtime in case the running platform (or a platform group containing it)
  /// is not specified. [other] will also be used if the platform is not yet
  /// supported by Flutter or this plugin. If [other] is not assigned, the
  /// function might be unsafe. See the last docstring paragraph.
  ///
  /// **Platform groups and subplatforms**
  ///
  /// To find out all the possible combinations, the
  /// [documentation](https://gitlab.com/mplavsic/pragmatic-platform-utils)
  /// should be consulted.
  ///
  /// **Scope**
  ///
  /// Use this function only for:
  ///
  /// * appearance (view)
  ///
  /// Since there is no other method to infer one of the above subplatforms
  /// other than screen size dimension, you should **NEVER** rely on this widget
  /// for business logic differences (such as the path to the database) between
  /// siblings platforms, e.g., [androidSmartphone] and [androidTablet]. The
  /// app dimension could change at runtime (app resizing), resulting in a
  /// platform switch, e.g., [androidSmartphone] to [androidTablet].
  ///
  /// **Remarks**
  ///
  /// This is an unsafe builder and should not be used unless all targeted
  /// platforms are given a builder. The safe constructor variant
  /// [AdaptivePlatformBuilder] enforces the assignment of [other]. If using
  /// the unsafe variant, i.e. this constructor, ensure all possible cases are
  /// already taken into account, i.e. there is no need to specify [other].
  const AdaptivePlatformBuilder.unsafe({
    Key? key,
    this.other,
    this.android,
    this.androidSmartphone,
    this.androidTablet,
    this.androidTabletSmall,
    this.androidTabletLarge,
    this.fuchsia,
    this.ios,
    this.iphone,
    this.ipad,
    this.ipadSmall,
    this.ipadLarge,
    this.linux,
    this.macos,
    this.windows,
    this.web,
    this.webSmartphone,
    this.webTablet,
    this.webTabletSmall,
    this.webTabletLarge,
    this.webDesktop,
    this.apple,
    this.desktop,
    this.mobile,
    this.tablet,
    this.tabletLarge,
    this.tabletSmall,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return unsafeAdaptivePlatformFunction(
        context: context,
        other: other,
        android: android,
        androidSmartphone: androidSmartphone,
        androidTablet: androidTablet,
        androidTabletSmall: androidTabletSmall,
        androidTabletLarge: androidTabletLarge,
        apple: apple,
        desktop: desktop,
        fuchsia: fuchsia,
        ios: ios,
        ipad: ipad,
        ipadSmall: ipadSmall,
        ipadLarge: ipadLarge,
        iphone: iphone,
        linux: linux,
        macos: macos,
        mobile: mobile,
        tablet: tablet,
        tabletLarge: tabletLarge,
        tabletSmall: tabletSmall,
        web: web,
        webDesktop: webDesktop,
        webTablet: webTablet,
        webTabletSmall: webTabletSmall,
        webTabletLarge: webTabletLarge,
        webSmartphone: webSmartphone,
        windows: windows,
      );
    });
  }

  /// Selects the widget builder to invoke based on the platform the
  /// application is currently running on.
  ///
  /// If provided, [other] will also be selected as default (fallback) at
  /// runtime in case the running platform (or a platform group containing it)
  /// is not specified. [other] will also be used if the platform is not yet
  /// supported by Flutter or this plugin. If [other] is not assigned, the
  /// function might be unsafe. See the last docstring paragraph.
  ///
  /// **Platform groups and subplatforms**
  ///
  /// To find out all the possible combinations, the
  /// [documentation](https://gitlab.com/mplavsic/pragmatic-platform-utils)
  /// should be consulted.
  ///
  /// **Scope**
  ///
  /// Use this function only for:
  ///
  /// * appearance (view)
  ///
  /// Since there is no other method to infer one of the above subplatforms
  /// other than screen size dimension, you should **NEVER** rely on this widget
  /// for business logic differences (such as the path to the database) between
  /// siblings platforms, e.g., [androidSmartphone] and [androidTablet]. The
  /// app dimension could change at runtime (app resizing), resulting in a
  /// platform switch, e.g., [androidSmartphone] to [androidTablet].
  const AdaptivePlatformBuilder({
    Key? key,
    required Widget Function() other,
    Widget Function()? android,
    Widget Function()? fuchsia,
    Widget Function()? ios,
    Widget Function()? linux,
    Widget Function()? macos,
    Widget Function()? windows,
    Widget Function()? web,
    Widget Function()? webTabletSmall,
    Widget Function()? webTabletLarge,
    Widget Function()? iphone,
    Widget Function()? ipad,
    Widget Function()? ipadSmall,
    Widget Function()? ipadLarge,
    Widget Function()? androidSmartphone,
    Widget Function()? androidTablet,
    Widget Function()? androidTabletSmall,
    Widget Function()? androidTabletLarge,
    Widget Function()? webSmartphone,
    Widget Function()? webTablet,
    Widget Function()? webDesktop,
    Widget Function()? tablet,
    Widget Function()? tabletSmall,
    Widget Function()? tabletLarge,
    Widget Function()? apple,
    Widget Function()? desktop,
    Widget Function()? mobile,
  }) : this.unsafe(
          other: other,
          android: android,
          androidSmartphone: androidSmartphone,
          androidTablet: androidTablet,
          apple: apple,
          desktop: desktop,
          fuchsia: fuchsia,
          ios: ios,
          ipad: ipad,
          iphone: iphone,
          linux: linux,
          macos: macos,
          mobile: mobile,
          tablet: tablet,
          web: web,
          webDesktop: webDesktop,
          webTablet: webTablet,
          webSmartphone: webSmartphone,
          windows: windows,
          androidTabletSmall: androidTabletSmall,
          androidTabletLarge: androidTabletLarge,
          ipadSmall: ipadSmall,
          ipadLarge: ipadLarge,
          tabletLarge: tabletLarge,
          tabletSmall: tabletSmall,
          webTabletSmall: webTabletSmall,
          webTabletLarge: webTabletLarge,
        );
}
