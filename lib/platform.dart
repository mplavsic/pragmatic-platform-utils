/// A simplified version of Platform.
library platform;

import 'dart:io' as standard;

import 'package:flutter/foundation.dart';

/// A simplified version of [standard.Platform] which brings pragmatism and
/// increased type safety to cross-platform development.
///
/// ### Checking order
///
/// Unlike with [standard.Platform], developers don't have to
/// consider the web case beforehand, i.e., they don't have to make the
/// typical distinction:
///
/// ```dart
/// if (kIsWeb) { // in typical Dart, this always gets checked first
///   return webValue;
/// } else {
///   if (Platform.isAndroid) {
///     return androidValue;
///   } ...
/// }
/// ```
///
/// Instead, web doesn't have to be distinguished immediately, e.g., it is
/// possible to write:
///
/// ```dart
/// if (Platform.isAndroid) {
///   return androidValue;
/// } else if (Platform.isWeb) { // web doesn't have to be checked first anymore
///   return webValue;
/// } ...
/// ```
///
/// ### Type safety
///
/// [standard.Platform] only offers [operatingSystem] to get information
/// about the running system.
/// ```dart
/// if (io.Platform.operatingSystem == "window") {...} // "s" is missing
/// ```
/// To prevent types such as the above one, this class offers
/// [getRunningPlatform] and [Platform] constants such as [windows].
/// ```dart
/// if (Platform.getRunningPlatform == windows) {...} // if windows is
/// // misspelled it will be highlighted and won't compile
/// ```
/// ### isOneOf
///
/// Multiple platforms no longer have to be checked the following way:
/// ```dart
/// if (kIsWeb || Platform.isWindows || Platform.isAndroid || Platform.isWeb) {
///   ...
/// }
/// ```
///
/// Thanks to [Platform.isOneOf], now it is all more elegant:
/// ```dart
/// if (Platform.isOneOf([windows, linux, android, web])) {...}
/// ```
/// ### Remarks
///
/// This class doesn't replace [standard.Platform] as to it does not include the
/// getters in [standard.Platform] (since they aren't supported for web). Use
/// web directly if you need to.
class Platform {
  /// Needed for [android], [fuchsia], [ios], [linux], [macos], [windows], [web]
  final String _name;

  /// Needed for [android], [fuchsia], [ios], [linux], [macos], [windows], [web]
  const Platform._(this._name);

  /// Needed for [android], [fuchsia], [ios], [linux], [macos], [windows], [web]
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Platform &&
          runtimeType == other.runtimeType &&
          _name == other._name;

  /// Needed for [android], [fuchsia], [ios], [linux], [macos], [windows], [web]
  @override
  int get hashCode => _name.hashCode;

  /// Whether the operating system is a version of
  /// [Linux](https://en.wikipedia.org/wiki/Linux).
  ///
  /// This value is `false` if the operating system is a specialized
  /// version of Linux that identifies itself by a different name,
  /// for example Android (see [isAndroid]).
  static final bool isLinux = !kIsWeb && standard.Platform.isLinux;

  /// Whether the operating system is a version of
  /// [macOS](https://en.wikipedia.org/wiki/MacOS).
  static final bool isMacOS = !kIsWeb && standard.Platform.isMacOS;

  /// Whether the operating system is a version of
  /// [Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows).
  static final bool isWindows = !kIsWeb && standard.Platform.isWindows;

  /// Whether the operating system is a version of
  /// [Android](https://en.wikipedia.org/wiki/Android_%28operating_system%29).
  static final bool isAndroid = !kIsWeb && standard.Platform.isAndroid;

  /// Whether the operating system is a version of
  /// [iOS](https://en.wikipedia.org/wiki/IOS).
  static final bool isIOS = !kIsWeb && standard.Platform.isIOS;

  /// Whether the operating system is a version of
  /// [Fuchsia](https://en.wikipedia.org/wiki/Google_Fuchsia).
  static final bool isFuchsia = !kIsWeb && standard.Platform.isFuchsia;

  /// Web is a platform not present in [standard.Platform].
  static const bool isWeb = kIsWeb;

  /// A string representing the operating system or platform.
  ///
  /// Possible values include:
  ///   "android"
  ///   "fuchsia"
  ///   "ios"
  ///   "linux"
  ///   "macos"
  ///   "windows"
  ///   "web"
  ///
  /// Note that this list may change over time so platform-specific logic
  /// should be guarded by the appropriate boolean getter e.g. [isMacOS].
  ///
  /// ### Warning
  ///
  /// Use this getter only when you need a string, e.g. displaying the string
  /// of the running platform, or when sending this information to a server.
  ///
  /// Avoid writing code that looks like this:
  ///
  /// ```dart
  /// final os = Platform.operatingSystem;
  /// switch (os) {
  ///   case "android":
  ///     return androidValue;
  ///   case "web":
  ///     return webValue;
  ///   case ...
  /// }
  /// ```
  ///
  /// Use [getRunningPlatform] instead, since it is type-safe.
  static String get operatingSystem =>
      kIsWeb ? 'web' : standard.Platform.operatingSystem;

  /// It can be only one of:
  ///
  /// - [android]
  /// - [fuchsia]
  /// - [ios]
  /// - [linux]
  /// - [macos]
  /// - [windows]
  /// - [web]
  ///
  /// If comparing the result, do not use `switch case`, instead use
  /// `if`/`else if`/`else` statements.
  ///
  /// ```dart
  /// final platform = Platform.getRunningPlatform;
  /// if (platform == android) {
  ///   ...
  /// } else if (platform == web) {
  ///   ...
  /// }
  /// ...
  /// ```
  static Platform get getRunningPlatform {
    switch (operatingSystem) {
      case "android":
        return android;
      case "fuchsia":
        return fuchsia;
      case "ios":
        return ios;
      case "linux":
        return linux;
      case "macos":
        return macos;
      case "windows":
        return windows;
      case "web":
        return web;
      default:
        return android;
    }
  }

  /// Checks if the currently running platform is one of [platforms].
  static bool isOneOf(List<Platform> platforms) =>
      platforms.any((p) => p == getRunningPlatform);
}

/// One of the supported platforms.
const Platform android = Platform._('android');

/// One of the supported platforms.
const Platform fuchsia = Platform._('fuchsia');

/// One of the supported platforms.
const Platform ios = Platform._('ios');

/// One of the supported platforms.
const Platform linux = Platform._('linux');

/// One of the supported platforms.
const Platform macos = Platform._('macos');

/// One of the supported platforms.
const Platform windows = Platform._('windows');

/// One of the supported platforms.
const Platform web = Platform._('web');
