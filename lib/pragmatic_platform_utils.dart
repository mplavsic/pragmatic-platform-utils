/// A library package that simplifies the cross-platform development process
/// by providing declarative and pragmatic instruments.
library pragmatic_platform_utils;

export 'src/adaptive_platform_builder.dart';
export 'src/platform_builder.dart';
export 'src/platform_function.dart';
