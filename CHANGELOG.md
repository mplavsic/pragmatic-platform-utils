## 4.0.0
* Include export of `platform_function.dart` inside the library `pragmatic_platform_utils` (it was left out by accident)
* Remove export of `adaptive_platform_function.dart` inside the library `pragmatic_platform_utils`
* Update documentation

## 3.0.0
* Use two libraries: `pragmatic_platform_utils` and `platform`.
* Add example
* Update documentation

## 2.0.0
* Add subplatforms such as `ipadSmall` and `ipadLarge`.
* Add more platform and subplatform groups such as `tabletSmall` and `ipad` (NB: `ipad` was previously a subplatform, but it is important to distinguish between small and big ones).
* Update documentation to match the state-of-the-art version.

## 2.0.0-dev.2
* Add documentation
* Rename AdvancedPlatformBuilder to `AdaptivePlatformBuilder`. Same for `advancedPlatformFunction`.

## 2.0.0-dev.1
* Complete rewrite. The documentation will be updated in the next dev version.
* Snippets are provided as a temporary documentation replacement.

## 1.0.0
* First release. It includes:
    * `PlatformFunction` class
    * `PlatformBuilder` widget
    * Pragmatic `Platform` class
